<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducation1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education1s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id1');
            $table->string('class1');
            $table->string('course_name1')->nullable();
            $table->string('board_university1');
            $table->string('year_of_passing1');
            $table->string('division1');
            $table->string('CGPA1')->nullable();
            $table->string('percentage1');
            $table->string('subjects1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education1s');
    }
}
