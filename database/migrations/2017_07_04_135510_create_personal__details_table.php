<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal__details', function (Blueprint $table) {
          $table->increments('id');
          $table->string('user_id');
          $table->string('previous_application');
          $table->string('father_name');
          $table->string('mother_name');
          $table->string('placeofbirth');
          $table->string('nationality');
          $table->string('gender');
          $table->string('status');
          $table->string('spouse');
          $table->string('catagory');
          $table->string('cat_certi_sn')->nullable();
          $table->string('other_cat_details')->nullable();
          $table->string('disability');
          $table->string('disability_blindst')->nullable();
          $table->string('disability_blindper')->nullable();
          $table->string('disability_blindcerti')->nullable();
          $table->string('disability_hearingst')->nullable();
          $table->string('disability_hearingper')->nullable();
          $table->string('disability_hearingcerti')->nullable();
          $table->string('disability_locomotorst')->nullable();
          $table->string('disability_locomotorper')->nullable();
          $table->string('disability_locomotorcerti')->nullable();
          $table->string('mailing_address');
          $table->string('permanent_address');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal__details');
    }
}
