<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExp15sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_15s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('agency');
            $table->string('host_institution');
            $table->string('from');
            $table->string('to');
            $table->string('duration');
            $table->string('sl_no_proof');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exp_15s');
    }
}
