<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducation2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education2s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id2');
            $table->string('class2');
            $table->string('course_name2')->nullable();
            $table->string('board_university2');
            $table->string('year_of_passing2');
            $table->string('title2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education2s');
    }
}
