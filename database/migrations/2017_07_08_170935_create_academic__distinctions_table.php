<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicDistinctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic__distinctions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('name_of_academy');
            $table->string('academic_distinction_obtained');
            $table->timestamps();
        });
    }

    /**
     * Reverse tan(arg)he migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic__distinctions');
    }
}
