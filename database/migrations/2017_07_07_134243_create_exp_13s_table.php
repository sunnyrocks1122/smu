<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExp13sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_13s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('designation');
            $table->string('pay_scale');
            $table->string('employer_details');
            $table->date('from_date');
            $table->date('to_date');
            $table->string('years_of_exp');
            $table->string('nature_of_work');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exp_13s');
    }
}
