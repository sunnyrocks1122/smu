@extends('layouts.form_nonav')
@section('title','Educational Qualification')
@section('content')
  <div class="container">
    <h1>12. Educational Qualifications</h1>
    <hr>
    @if(Session::has('message'))
          <center><p class="alert {{ Session::get('alert-class', 'alert-danger') }}" id="msg">{{ Session::get('message') }}</p></center>
    @endif
    <div class="row" style="color:black;">
      <div class="col-lg-3 well">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
          <li><a href="{{url('/personal')}}">Personal</a></li>
          <li class="active"><a href="{{url('/educational')}}">Educational Details</a></li>
          <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
          <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
          <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
          <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
          <li><a href="{{url('/referee')}}">Referees</a></li>
          <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
          <li><a href="{{url('/declaration')}}">Declaration</a></li>
          <li><a href="{{url('/final_print')}}">Final Print</a></li>
        </ul>
      </div>
      <div class="col-sm-9">
        
        <h4>Education details Upto M.Ed</h4>
        @if(($edu1s))
          <table class="table table-bordered">
            <tr>
              <th>Class</th><th>Name of Course</th><th>Name of the Board/University</th><th>Year of Passing</th><th>Division</th><th>CGPA (if grading is applicable)</th><th>Precentage of Marks</th><th>Subject Studied</th>
            </tr>
              @foreach ($edu1s as $edu1)
                <tr>
                  <td>{{$edu1->class1}}</td>
                  <td>{{$edu1->course_name1}}</td>
                  <td>{{$edu1->board_university1}}</td>
                  <td>{{$edu1->year_of_passing1}}</td>
                  <td>{{$edu1->division1}}</td>
                  <td>{{$edu1->CGPA1}}</td>
                  <td>{{$edu1->percentage1}}</td>
                  <td>{{$edu1->subjects1}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        
        <hr>
        <form class="form-horizontal" method="post" action="{{url('/submit_edu1')}}">
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Class</label>
            <div class="col-sm-6">
              <select class="form-control" name="class1" required>
                <option value="">Please select</option>
                <option value="10th Class/equivalent">10th Class/equivalent</option>
                <option value="10+2/Higher Secondary equivalent">10+2/Higher Secondary equivalent</option>
                <option value="Bachelor's Degree">Bachelor's Degree</option>
                <option value="Master's Degree">Master's Degree</option>
                <option value="B. Ed. (Special Education)">B. Ed. (Special Education)</option>
                <option value="M. Ed. (Special Education)">M. Ed. (Special Education)</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Name of Course</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="course_name" name="course_name1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Name of the Board/University</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="board_university" name="board_university1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Year of Passing</label>
            <div class="col-sm-6">
              <input type="number" min="0" class="form-control" required id="year_of_passing" name="year_of_passing1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Division</label>
            <div class="col-sm-6">
              <input type="number" min="0" class="form-control" required id="division" name="division1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">CGPA (if grading is applicable)</label>
            <div class="col-sm-6">
              <input type="number" min="0" class="form-control" id="CGPA" name="CGPA1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Precentage of Marks</label>
            <div class="col-sm-6">
              <input type="number" min="0" class="form-control" required id="percentage" name="percentage1">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Subject Studied</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="subjects" name="subjects1">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <button type="submit" class="btn btn-block btn-success">Add</button>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
        <h4>Education details Upto Ph. D./D. Phil.</h4>
        @if(($edu2s))
          <table class="table table-bordered">
            <tr>
              <th>Class</th><th>Name of Course</th><th>Name of the Board/University</th><th>Year of Passing</th><th>Title</th>
            </tr>
              @foreach ($edu2s as $edu2)
                <tr>
                  <td>{{$edu2->class2}}</td><td>{{$edu2->course_name2}}</td><td>{{$edu2->board_university2}}</td>
                  <td>{{$edu2->year_of_passing2}}</td>
                  <td>{{$edu2->title2}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        <hr>
        <form class="form-horizontal" method="post" action="{{url('/submit_edu2')}}">
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Class</label>
            <div class="col-sm-6">
              <select class="form-control" name="class2" required>
                <option value="">Please select</option>
                <option value="M. Phil.">M. Phil.</option>
                <option value="Ph. D./D. Phil.">Ph. D./D. Phil.</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Name of Course</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="course_name" name="course_name2">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Name of the Board/University</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="board_university" name="board_university2">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Year of Passing</label>
            <div class="col-sm-6">
              <input type="number" class="form-control" required id="year_of_passing" name="year_of_passing2">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Title</label>
            <div class="col-sm-6">
              <input type="text min="0" class="form-control" required id="title" name="title2">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <button type="submit" class="btn btn-block btn-success">Add</button>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
        <h4>NET/SLET/SET and Other Educational Details</h4>
        @if(($edu2s))
          <table class="table table-bordered">
            <tr>
              <th>Subject</th><th>Roll. No.</th><th>Year</th>
            </tr>
              @foreach ($edu3s as $edu3)
                <tr>
                  <td>{{$edu3->subject3}}</td><td>{{$edu3->rollno3}}</td>
                  <td>{{$edu3->year_of_passing3}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        <hr>
        <form class="form-horizontal" method="post" action="{{url('/submit_edu3')}}">
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Class</label>
            <div class="col-sm-6">
              <select class="form-control" name="class2" required>
                <option value="">Please select</option>
                <option value="NET/SLET/SET">NET/SLET/SET</option>
                <option value="Any Other Exam Passed">Any Other Exam Passed</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Subject</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="course_name" name="subject3">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Roll No.</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" required id="board_university" name="rollno3">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Year of Passing</label>
            <div class="col-sm-6">
              <input type="number" class="form-control" required id="year_of_passing" name="year_of_passing3">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <button type="submit" class="btn btn-block btn-success">Add</button>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <hr class="featurette-divider">
@endsection
