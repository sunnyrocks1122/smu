@extends('layouts.form_nonav')
@section('title','Registration')
@section('content')
  <h1 align="center">Applicant Registration</h1>
  <hr>
  <div class="row" style="color:black;font-size:16px">
    <div class="col-lg-9">
      <form class="form-horizontal" method="post" action="{{ url('/create_new_user') }}">
        <div class="form-group">
          <label for="inputPost" class="col-sm-4 control-label">Post Applied For</label>
          <div class="col-sm-8">
            <select class="form-control" id="post_applied_for" name="post_applied_for">
              <option value="">Please Select Post</option>
              <option value="Assistant Professor">Assistant Professor</option>
              <option value="Associate Professor">Associate Professor</option>
              <option value="Professor">Professor</option>
            </select>
          </div>
        </div>        
        <div class="form-group">
          <label for="inputDepartment" class="col-sm-4 control-label">Department/Center</label>
          <div class="col-sm-8" id="pro">
            <select required class="form-control" id="department" name="department" placeholder="Department">
              <option value="0">Please select Department</option>
              <option value="Social WorkEconomics">Social Work</option>
              <option value="Philosophy">Philosophy</option>
              <option value="Library Science">Library Science</option>
              <option value="Geography">Geography</option>
              <option value="Psychology">Psychology</option>
              <option value="Information Technology">Information Technology</option>
              <option value="Botany">Botany</option>
              <option value="Botany">Zoology</option>
              <option value="Chemistry">Chemistry</option>
              <option value="Mathematics">Mathematics</option>
              <option value="Bio-Technology">Bio-Technology</option>
              <option value="Microbiology">Microbiology</option>
            </select>
          </div>
          <div class="col-sm-8" id="a_pro">
            <select required class="form-control" id="department" name="department" placeholder="Department">
              <option value="0">Please select Department</option>
              <option value="Social WorkEconomics">Social Work</option>
              <option value="Philosophy">Philosophy</option>
              <option value="Library Science">Library Science</option>
              <option value="Geography">Geography</option>
              <option value="Psychology">Psychology</option>
              <option value="Information Technology">Information Technology</option>
              <option value="Botany">Botany</option>
              <option value="Botany">Zoology</option>
              <option value="Chemistry">Chemistry</option>
              <option value="Mathematics">Mathematics</option>
              <option value="Bio-Technology">Bio-Technology</option>
              <option value="Microbiology">Microbiology</option>
            </select>
          </div>
          <div class="col-sm-8" id="ass_pro">
            <select required class="form-control" id="department" name="department" placeholder="Department">
              <option value="0">Please select Department</option>
              <option value="Social WorkEconomics">Social Work</option>
              <option value="Philosophy">Philosophy</option>
              <option value="Library Science">Library Science</option>
              <option value="Geography">Geography</option>
              <option value="Psychology">Psychology</option>
              <option value="Information Technology">Information Technology</option>
              <option value="Botany">Botany</option>
              <option value="Botany">Zoology</option>
              <option value="Chemistry">Chemistry</option>
              <option value="Mathematics">Mathematics</option>
              <option value="Bio-Technology">Bio-Technology</option>
              <option value="Microbiology">Microbiology</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="inputPostNature" class="col-sm-4 control-label">Nature of the Post</label>
          <div class="col-sm-8">
            <select class="form-control" id="nature_of_post" name="nature_of_post">
              <option value="0">Please Select an Option</option>
              <option value="Specialized">Specialized</option>
              <option value="Non-Specialized">Non-Specialized</option>
              <option value="Both">Both</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="inputName" class="col-sm-4 control-label" >Name</label>
          <div class="col-sm-3">
            <input type="text" required class="form-control" style="text-transform:uppercase" id="first_name" name="first_name" placeholder="First Name">
          </div>
          <div class="col-sm-2">
            <input type="text" class="form-control" id="middle_name" style="text-transform:uppercase" name="middle_name" placeholder="Middle Name">
          </div>
          <div class="col-sm-3">
            <input type="text" required class="form-control" id="last_name" style="text-transform:uppercase" name="last_name" placeholder="Last Name">
          </div>
        </div>
        <div class="form-group">
          <label for="inputDOB" class="col-sm-4 control-label">Date of Birth</label>
          <div class="col-sm-8">
            <input type="text" id="dob" required class="form-control" name="dob">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Email</label>
          <div class="col-sm-8">
            <input type="email" class="form-control" id="email" name="email">
          </div>
        </div>
        <div class="form-group">
          <label for="inputNumber" class="col-sm-4 control-label">Mobile Number</label>
          <div class="col-sm-8">
            <input type="number" required class="form-control" id="contact" name="contact" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-10 col-sm-2">
            <button type="submit" class="btn btn-lg btn-success">Register</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
    </div>
    <div class="col-lg-3 well">
      <h3>Already Registered</h3>
      <a href="{{ url('/login_form')}}"><button class="btn btn-success">Log In</button></a>
    </div>
    </div>
  <hr class="featurette-divider">
  @endsection
