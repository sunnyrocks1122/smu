@extends('layouts.form_nonav')
@section('title','Log In')
@section('content')
  <h1 align="center">Applicant Login</h1>
  <hr>
  <div class="row" style="color:black;font-size:16px">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
      @if(Session::has('message'))
          <center><p class="alert {{ Session::get('alert-class', 'alert-danger') }}" id="msg11">{{ Session::get('message') }}</p></center>
      @endif
      <form class="form-horizontal" method="post" action="{{ url('/user_login_submit') }}">
        <div class="form-group">
          <label for="inputNumber" class="col-sm-4 control-label">Registration Number</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="user_id" name="user_id">
          </div>
        </div>
        <div class="form-group">
          <label for="inputCNumber" class="col-sm-4 control-label">Password</label>
          <div class="col-sm-8">
            <input type="password" required class="form-control" name="password" id="password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-4 col-sm-2">
            <button type="submit" class="btn btn-lg btn-success">Login</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
    </div>
  </div>
  <hr class="featurette-divider">
  @endsection
