@extends('layouts.form_nonav')
@section('title','Personal')
@section('content')
<div class="container">
  <h1>Personal Details</h1>
  <hr>
  @if(Session::has('message'))
          <center><p class="alert {{ Session::get('alert-class', 'alert-danger') }}" id="msg">{{ Session::get('message') }}</p></center>
  @endif
  <div class="row" style="color:black;">
    <div class="col-lg-3 well">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
        <li class="active"><a href="{{url('/personal')}}">Personal</a></li>
        <li><a href="{{url('/educational')}}">Educational Details</a></li>
        <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
        <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
        <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
        <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
        <li><a href="{{url('/referee')}}">Referees</a></li>
        <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
        <li><a href="{{url('/declaration')}}">Declaration</a></li>
        <li><a href="{{url('/final_print')}}">Final Print</a></li>
      </ul>
    </div>
    @if(!($personal))
      <div class="col-lg-9">
          <form class="form-horizontal" action="{{url('/postProfile')}}" method="post">
            <div class="form-group">
              <label for="inputPreRecord" class="col-sm-6 control-label">Have you applied earlier ? Advt. No. 19,21</label>
              <div class="col-sm-6">
                <input type="radio" required name="previous_application" id="prerecord1" value="Yes">Yes
                <input type="radio" required name="previous_application" id="prerecord2" value="No">No
              </div>
            </div>
            <div class="form-group">
              <label for="inputFatherName" class="col-sm-4 control-label">Father's Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" required id="father_name" name="father_name" placeholder="Father's Name">
              </div>
            </div>
            <div class="form-group">
              <label for="inputMotherName" class="col-sm-4 control-label">Mother's Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" required id="mother_name" name="mother_name" placeholder="Mother's Name">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-4 control-label">Place of Birth</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" required id="placeofbirth" name="placeofbirth" placeholder="Place of Birth">
              </div>
            </div>
            <div class="form-group">
              <label for="inputDOB" class="col-sm-4 control-label">Nationality</label>
              <div class="col-sm-8">
                <select required class="form-control" id="nationality" name="nationality">
                  <option value="0">Please Select an Option</option>
                  <option value="Indian">Indian</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-4 control-label">Gender</label>
              <div class="col-sm-8">
                <input type="radio" required name="gender" id="inputGender" value="Male">Male
                <input type="radio" required name="gender" id="inputGender" value="Female">Female
                <input type="radio" required name="gender" id="inputGender" value="Transgender">Transgender
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-4 control-label">Marital Status</label>
              <div class="col-sm-6">
                <input type="radio" required name="status" id="inputStatus" value="Married">Married
                <input type="radio" required name="status" id="inputStatus" value="Unmarried">Unmarried
                <input type="radio" required name="status" id="inputStatus" value="Divorced">Divorced
              </div>
              <div class="col-sm-2" id="#name_spouse">
                Name of Spouse<input type="text" required name="spouse" class="form-control" id="inputStatus">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-4 control-label">Catagory</label>
              <div class="col-sm-6">
                <input type="radio" required name="catagory" id="GEN" value="GEN">GEN
                <input type="radio" required name="catagory" id="SC" value="SC/ST">SC/ST
                <input type="radio" required name="catagory" id="OBC" value="OBC">OBC
                <input type="radio" required name="catagory" id="other" value="other">Other
                <div id="cat_sc">Enclosure
                  <input type="text" name="cat_certi_sn" class="form-control" id="inputCatagoryCerti" placeholder="Details">
                </div>
                <div id="cat_others">If other, please mention details
                  <input type="text" name="other_cat_details" class="form-control" id="inputCatotherdetails" placeholder="Details">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-4 control-label">Person with Disability</label>
              <div class="col-sm-6">
                <input type="radio" required name="disability" id="disable_y" value="Yes">Yes
                <input type="radio" required name="disability" id="disable_n" value="No">No
              </div>
                <div id="disability_options">
                  <div class="form-group col-sm-8">
                    <table class="table table-bordered">
                      <tr>
                        <td>Type of Disability</td><td>Yes/No</td><td>Percentage of disability</td><td>S. No. of proof of enclosure</td>
                      </tr>
                      <tr>
                        <td>Blindness or Low Vision</td><td><input type="radio" name="disability_blindst" value="Blindness or Low Vision">Yes <input type="radio" name="disability_blindst" value="No">No</td><td><input type="number" class="form-control" min="1" step="1" max="100" name="disability_blindper" id="inputFname"></td><td><input type="text" class="form-control" name="disability_blindcerti" id="inputFname"></td>
                      </tr>
                      <tr>
                        <td>Hearing Impairment</td><td><input type="radio" name="disability_hearingst" value="Hearing Impairment">Yes <input type="radio" name="disability_hearingst" value="Hearing No">No </td><td><input type="number" min="1" step="1" max="100" name="disability_hearingper" class="form-control" id="inputFname"></td><td><input type="text" name="disability_hearingcerti" class="form-control" id="inputFname"></td>
                      </tr>
                      <tr>
                        <td>Locomotor disability or cerebral palsy (includes all cases of Orthopeadically Disabled)</td><td><input type="radio" name="disability_locomotorst" value="Locomotor disability or cerebral palsy">Yes <input type="radio" name="disability_locomotorst" value="No"> No</td><td><input type="number" min="1" step="1" max="100" name="disability_locomotorper" class="form-control" id="inputFname"></td><td><input type="text" name="disability_locomotorcerti" class="form-control" id="inputFname"></td>
                      </tr>
                    </table>
                  </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNumber" class="col-sm-4 control-label">Mailing Address</label>
              <div class="col-sm-8">
                <textarea class="form-control" required id="mailing_address" name="mailing_address"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNumber" class="col-sm-4 control-label">Same as mailing address</label>
              <div class="col-sm-8">
                <input type="checkbox" id="same_as_mailing" name="same_as_mailing" value="Yes">
              </div>
            </div>
            <div class="form-group">
              <label for="inputNumber" class="col-sm-4 control-label">Permanent Address</label>
              <div class="col-sm-8">
                <textarea class="form-control" required id="permanent_address" name="permanent_address"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-10 col-sm-2">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
                <button type="submit" class="btn btn-block btn-success">Submit</button>
              </div>
            </div>
          </form>
        </div>
    @else
        <div class="col-lg-9">
        <form class="form-horizontal" action="{{url('/postProfile')}}" method="post">
          <div class="form-group">
            <label for="inputPreRecord" class="col-sm-6 control-label">Have you applied earlier ? Advt. No. 19,21</label>
            <div class="col-sm-6">
              <input type="radio" required name="previous_application" id="prerecord1" value="yes">Yes
              <input type="radio" required name="previous_application" id="prerecord2" value="no">No
            </div>
          </div>
          <div class="form-group">
            <label for="inputFatherName" class="col-sm-4 control-label">Father's Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" required id="father_name" name="father_name" value="{{$personal->father_name}}" placeholder="Father's Name">
            </div>
          </div>
          <div class="form-group">
            <label for="inputMotherName" class="col-sm-4 control-label">Mother's Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" required id="mother_name" name="mother_name" value="{{$personal->mother_name}}" placeholder="Mother's Name">
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-4 control-label">Place of Birth</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" required id="placeofbirth" name="placeofbirth" value="{{$personal->placeofbirth}}" placeholder="Place of Birth">
            </div>
          </div>
          <div class="form-group">
            <label for="inputDOB" class="col-sm-4 control-label">Nationality</label>
            <div class="col-sm-8">
              <select required class="form-control" id="nationality" name="nationality">
                <option value="0">Please Select an Option</option>
                <option value="Indian">Indian</option>
                <option value="Other">Other</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Gender</label>
            <div class="col-sm-8">
              <input type="radio" required name="gender" id="inputGender" value="Male">Male
              <input type="radio" required name="gender" id="inputGender" value="Female">Female
              <input type="radio" required name="gender" id="inputGender" value="Transgender">Transgender
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Marital Status</label>
            <div class="col-sm-6">
              <input type="radio" required name="status" id="inputStatus" value="Married">Married
              <input type="radio" required name="status" id="inputStatus" value="Unmarried">Unmarried
              <input type="radio" required name="status" id="inputStatus" value="Divorced">Divorced
            </div>
            <div class="col-sm-2" id="#name_spouse">
              <input type="text" required name="spose" class="form-control" id="inputStatus" value="{{$personal->spouse}}">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Catagory</label>
            <div class="col-sm-6">
              <input type="radio" required name="catagory" id="GEN" value="GEN">GEN
              <input type="radio" required name="catagory" id="SC" value="SC/ST">SC/ST
              <input type="radio" required name="catagory" id="OBC" value="OBC">OBC
              <input type="radio" required name="catagory" id="other" value="other">Other
              <div id="cat_sc">Enclosure
                <input type="text" name="cat_certi_sn" class="form-control" id="inputCatagoryCerti" placeholder="Details">
              </div>
              <div id="cat_others">If other, please mention details
                <input type="text" name="other_cat_details" class="form-control" id="inputCatotherdetails" placeholder="Details">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Person with Disability</label>
            <div class="col-sm-6">
              <input type="radio" required name="disability" id="disable_y" value="y">Yes
              <input type="radio" required name="disability" id="disable_n" value="n">No
            </div>
              <div id="disability_options">
                <div class="form-group col-sm-8">
                  <table class="table table-bordered">
                    <tr>
                      <td>Type of Disability</td><td>Yes/No</td><td>Percentage of disability</td><td>S. No. of proof of enclosure</td>
                    </tr>
                    <tr>
                      <td>Blindness or Low Vision</td><td><input type="radio" name="disability_blindst" value="Blindness or Low Vision">Yes <input type="radio" name="disability_blindst" value="No">No</td><td><input type="number" class="form-control" min="1" step="1" max="100" name="disability_blindper" id="inputFname"></td><td><input type="text" class="form-control" name="disability_blindcerti" id="inputFname"></td>
                    </tr>
                    <tr>
                      <td>Hearing Impairment</td><td><input type="radio" name="disability_hearingst" value="Hearing Impairment">Yes <input type="radio" name="disability_hearingst" value="Hearing No">No </td><td><input type="number" min="1" step="1" max="100" name="disability_hearingper" class="form-control" id="inputFname"></td><td><input type="text" name="disability_hearingcerti" class="form-control" id="inputFname"></td>
                    </tr>
                    <tr>
                      <td>Locomotor disability or cerebral palsy (includes all cases of Orthopeadically Disabled)</td><td><input type="radio" name="disability_locomotorst" value="Locomotor disability or cerebral palsy">Yes <input type="radio" name="disability_locomotorst" value="No"> No</td><td><input type="number" min="1" step="1" max="100" name="disability_locomotorper" class="form-control" id="inputFname"></td><td><input type="text" name="disability_locomotorcerti" class="form-control" id="inputFname"></td>
                    </tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="form-group">
            <label for="inputNumber" class="col-sm-4 control-label">Mailing Address</label>
            <div class="col-sm-8">
              <textarea class="form-control" required id="mailing_address" name="mailing_address">{{$personal->mailing_address}}</textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="inputNumber" class="col-sm-4 control-label">Same as mailing address</label>
            <div class="col-sm-8">
              <input type="checkbox" id="same_as_mailing" name="same_as_mailing" value="Yes">
            </div>
          </div>
          <div class="form-group">
            <label for="inputNumber" class="col-sm-4 control-label">Permanent Address</label>
            <div class="col-sm-8">
              <textarea class="form-control" required id="permanent_address" name="permanent_address">{{$personal->permanent_address}}</textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <input type="hidden" name="_token" value="{{ Session::token() }}">
                <button type="submit" class="btn btn-block btn-success">Update</button>
            </div>
          </div>
        </form>
      </div>    
    @endif
    
  </div>
  <hr class="featurette-divider">
  </div>
@endsection
