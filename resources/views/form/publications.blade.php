@extends('layouts.form_nonav')
@section('title','Research Publications')
@section('content')
<div class="container">
  <h1 align="center">RESEARCH, PUBLICATIONS AND ACADEMIC CONTRIBUTIONS</h1>
  <hr>
  <div class="row" style="color:black;">
    <div class="col-lg-3 well">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
        <li><a href="{{url('/personal')}}">Personal</a></li>
        <li><a href="{{url('/educational')}}">Educational Details</a></li>
        <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
        <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
        <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
        <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
        <li><a href="{{url('/referee')}}">Referees</a></li>
        <li class="active"><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
        <li><a href="{{url('/declaration')}}">Declaration</a></li>
        <li><a href="{{url('/final_print')}}">Final Print</a></li>
      </ul>
    </div>
    <div class="col-lg-9">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-10"><h4>A. Published Papers in Journals</h4></div>
            <div class="col-lg-2">
              <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#papers">Add</button>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              @if(($data_18as))
                <table class="table table-bordered">
                    <tr>
                      <td>Title with Page No.</td>
                      <td>Journal</td>
                      <td>ISSN/ISBN No.</td>
                      <td>Impact Factor</td>
                      <td>Whether Peer reviewed ? Impact factor, if any</td>
                      <td>Number of co-authors</td>
                      <td>Whether you are the first or corresponding author ?</td>
                      <td>Score</td>
                    </tr>
                    @foreach ($data_18as as $data_18a)
                      <tr>
                        <td>{{$data_18a->title}}</td>
                        <td>{{$data_18a->journal}}</td>
                        <td>{{$data_18a->issn}}</td>
                        <td>{{$data_18a->impact}}</td>
                        <td>{{$data_18a->peer_review}}</td>
                        <td>{{$data_18a->co_authors}}</td>
                        <td>{{$data_18a->first_author}}</td>
                        <td>{{$data_18a->score}}</td>
                      </tr>
                    @endforeach
                </table>
              @endif              
            </div>
          </div>
        </div>
      </div>    
      <div class="modal fade bs-example-modal-lg" id="papers" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Published Papers in Journals</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{url('/post_18a')}}">
            <div class="modal-body">
              <div class="row" style="color:black;font-size:16px">
                  <div class="col-lg-10">
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="title18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Journal</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="journal18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="issn18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">	Impact factor</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="impact18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed ? Impact factor, if any</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="peer_review18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="co_author18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Whether you are the  first or corresponding author ?</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" required id="inputFname" name="first_author18a">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                          <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                        <div class="col-sm-8">
                          <input type="number" min="0" class="form-control"  required id="inputFname" name="score18a">
                        </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Save</button></a>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    <hr>
    <div class="row">
      <div class="col-lg-10">
        <h4>B. Articles/ Chapters published in Books</h4>
      </div>
      <div class="col-lg-2">
        <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#articles">Add</button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        @if(($data_18bs))
          <table class="table table-bordered">
              <tr>
                <td>Title with Page No.</td>
                <td>Title of the Book</td>
                <td>ISSN/ISBN No.</td>
                <td>Whether Peer reviewed ?</td>
                <td>Number of co-authors</td>
                <td>Whether you are the first or corresponding author ?</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18bs as $data_18b)
                <tr>
                  <td>{{$data_18b->title}}</td>
                  <td>{{$data_18b->book_title}}</td>
                  <td>{{$data_18b->issn}}</td>
                  <td>{{$data_18b->peer_review}}</td>
                  <td>{{$data_18b->co_authors}}</td>
                  <td>{{$data_18b->first_author}}</td>
                  <td>{{$data_18b->score}}</td>
                </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="articles" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Articles/ Chapters published in Books</h4>
        </div>
        <form class="form-horizontal" method="post" action="{{url('/post_18b')}}">
        <div class="modal-body">
          
            <div class="row" style="color:black;font-size:20px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page Nos.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="title18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Book Title, editor & publisher</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="book_title18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="issn18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed ?</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="peer_review18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="co_authors18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the  first or corresponding author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" required id="inputFname" name="first_author18b">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                      <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" required id="inputFname" name="score18b">
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>C. Full papers in Conference Proceedings</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#papers_pre">Add Papers</button>
    </div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        @if(($data_18cs))
          <table class="table table-bordered">
              <tr>
                <td>Title with Page No.</td>
                <td>Details of Conference Publication</td>
                <td>ISSN/ISBN No.</td>
                <td>Number of co-authors</td>
                <td>Whether you are the main author ?</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18cs as $data_18c)
              <tr>
                <td>{{$data_18c->title}}</td>
                <td>{{$data_18c->conf_publication}}</td>
                <td>{{$data_18c->issn}}</td>
                <td>{{$data_18c->co_authors}}</td>
                <td>{{$data_18c->main_author}}</td>
                <td>{{$data_18c->score}}</td>
              </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="papers_pre" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form class="form-horizontal" method="post" action="{{url('/post_18c')}}">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Full papers in Conference Proceedings</h4>
        </div>
        <div class="modal-body">
          <div class="row" style="color:black;font-size:20px">
            <div class="col-lg-10">
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" required id="inputFname" name="title18c">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Details of Conference Publication</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="conf_publication18c">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="issn18c">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="co_authors18c">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Whether you are the main author</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="main_author18c">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                  <div class="col-sm-8">
                    <input type="number" min="0" class="form-control" id="inputFname" required name="score18c">
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>D. Books Published as author or as editor</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#books">Add</button>
    </div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        @if(($data_18ds))
          <table class="table table-bordered">
              <tr>
                <td>Title with Page No.</td>
                <td>Type of book and authorship</td>
                <td>Publisher & ISSN/ ISBN No</td>
                <td>Whether Peer reviewed</td>
                <td>Number of co-authors</td>
                <td>Whether you are the main author ?</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18ds as $data_18d)
              <tr>
                <td>{{$data_18d->title}}</td>
                <td>{{$data_18d->type_of_book}}</td>
                <td>{{$data_18d->issn}}</td>
                <td>{{$data_18d->peer_review}}</td>
                <td>{{$data_18d->co_authors}}</td>
                <td>{{$data_18d->main_author}}</td>
                <td>{{$data_18d->score}}</td>
              </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="books" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form class="form-horizontal" action="{{url('/post_18d')}}" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Books Published as author or as editor</h4>
        </div>
        <div class="modal-body">
          <div class="row" style="color:black;">
            <div class="col-lg-10">
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Title with Page No.</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="title18d">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Type of book and authorship</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="type_of_authorship18d">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Publisher & ISSN/ ISBN No.</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="issn18d">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="peer_review18d">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="co_authors18d">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Whether you are the main author</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputFname" required name="main_author18d">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                  <div class="col-sm-8">
                    <input type="number" min="0" class="form-control" id="inputFname" required name="score18d">
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>E. Ongoing Projects/ Consultancies</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#projects">Add</button>
    </div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        @if(($data_18cs))
          <table class="table table-bordered">
              <tr>
                <td>Title with Page No.</td>
                <td>Organization</td>
                <td>Period</td>
                <td>Grant/ Amount Mobilized (Rs. Lakh)</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18cs as $data_18c)
              <tr>
                <td>{{$data_18c->title}}</td>
                <td>{{$data_18c->organization}}</td>
                <td>{{$data_18c->period}}</td>
                <td>{{$data_18c->grant}}</td>
                <td>{{$data_18c->score}}</td>
              </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="projects" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
      <form class="form-horizontal" method="post" action="{{url('/post_18e')}}">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Ongoing Projects/ Consultancies</h4>
        </div>
        <div class="modal-body">
            <div class="row" style="color:black;">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="title18e">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organization</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="organization18e">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Period</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="period18e">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Grant/ Amount Mobilized (Rs. Lakh)</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="grant18e">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                      <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname" required name="score18e">
                    </div>
                  </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>F. Projects Outcome/Outputs</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        @if(($data_18fs))
          <table class="table table-bordered">
              <tr>
                <td>Title</td>
                <td>Organization</td>
                <td>Period</td>
                <td>Grant/ Amount Mobilized (Rs. Lakh)</td>
                <td>Whether policy document/ patent as outcome</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18fs as $data_18f)
              <tr>
                <td>{{$data_18f->title}}</td>
                <td>{{$data_18f->organization}}</td>
                <td>{{$data_18f->period}}</td>
                <td>{{$data_18f->grant}}</td>
                <td>{{$data_18f->policy_doc_as_outcome}}</td>
                <td>{{$data_18f->score}}</td>
              </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
      <form class="form-horizontal" action="post_18f" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Projects Outcome/Outputs</h4>
        </div>
        <div class="modal-body">
            <div class="row" style="color:black;">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="title18f">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organization</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="organization18f">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Period</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="period18f">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Grant/ Amount Mobilized (Rs. Lakh)</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="grant18f">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether policy document/ patent as outcome</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="policy_doc18f">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                      <p class="text text-warning">*Please refer to excel sheet ‘score sheet for assistant professors’ for filling the Score</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname" required name="score18f">
                    </div>
                  </div>
              </div>
            </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>G. Paper presented in Conferences, Seminars, Workshops, Symposiam (mention only upto a maximum of Ten [10])</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#paper_presented">Add</button>
    </div>
  </div>
  <div class="row">
      <div class="col-lg-12">
        @if(($data_18gs))
          <table class="table table-bordered">
              <tr>
                <td>Title of the Paper Presented</td>
                <td>Title of Conference / Seminar</td>
                <td>Organized by</td>
                <td>Type of Conference</td>
                <td>Score</td>
              </tr>
              @foreach ($data_18gs as $data_18g)
              <tr>
                <td>{{$data_18g->title}}</td>
                <td>{{$data_18g->title_of_conference}}</td>
                <td>{{$data_18g->organizer}}</td>
                <td>{{$data_18g->type}}</td>
                <td>{{$data_18g->score}}</td>
              </tr>
              @endforeach
          </table>
        @endif              
      </div>
    </div>
  <div class="modal fade bs-example-modal-lg" id="paper_presented" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <form class="form-horizontal" method="post" action="{{url('/post_18g')}}">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Paper presented in Conferences, Seminars, Workshops, Symposia</h4>
        </div>
        <div class="modal-body">
            <div class="row" style="color:black;">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title of the Paper Presented </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="title18g">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title of Conference / Seminar</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="title_of_conf18g">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organized by</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname" required name="organizer18g">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Type of Conference</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="inputFname" required name="type_of_conf18g">
                        <option value="">Please select Type</option>
                        <option value="International">International</option>
                        <option value="National">National</option>
                        <option value="State">State</option>
                        <option value="Regional">Regional</option>
                        <option value="College or University level">College or University level</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname" required name="score18g">
                    </div>
                  </div>
              </div>
            </div>          
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <input type="hidden" name="_token" value="{{ Session::token() }}">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <hr>
</div>
</div>
  <hr>
</div>
@endsection
