@extends('layouts.form_nonav')
@section('title','Post doctoral Experience Details')
@section('content')
  <div class="container">
    <h1 align="center">Post doctoral Experience Details</h1>
    <hr>
    <div class="row" style="color:black;">
          <div class="col-lg-3 well">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
              <li><a href="{{url('/personal')}}">Personal</a></li>
              <li><a href="{{url('/educational')}}">Educational Details</a></li>
              <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
              <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
              <li class="active"><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
              <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
              <li><a href="{{url('/referee')}}">Referees</a></li>
              <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
              <li><a href="{{url('/declaration')}}">Declaration</a></li>
              <li><a href="{{url('/final_print')}}">Final Print</a></li>
            </ul>
          </div>
          <div class="col-sm-9">
            @if(($exp15s))
            <table class="table table-bordered">
              <tr>
                <th>Agency</th><th>Host Institution</th><th>From</th><th>To</th><th>Duration</th>
              </tr>
                @foreach ($exp15s as $exp15)
                  <tr>
                    <td>{{$exp15->agency}}</td>
                    <td>{{$exp15->host_institution}}</td>
                    <td>{{$exp15->from}}</td>
                    <td>{{$exp15->to}}</td>
                    <td>{{$exp15->duration}}</td>
                  </tr>
                @endforeach
            </table>
          @endif
          
          <form class="form-horizontal" method="post" action="{{url('/post_exp15')}}">
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Agency</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="agency" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Host Institution</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="host_institution" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">From</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="from" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">To</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="to" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Duration</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="duration" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">S. No. of proof of enclosure</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputFname" name="sl_no_proof">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <button type="submit" class="btn btn-block btn-success">Add</button></a>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
      </div>
    </div>
    <hr class="featurette-divider">
  </div>
@endsection
