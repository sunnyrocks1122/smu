@extends('layouts.form_nonav')
@section('title','Chronological list of Experience')
@section('content')
  <div class="container">
    <h1 align="center">Chronological list of Experience</h1>
  <hr>
  <div class="row" style="color:black;">
      <div class="col-lg-3 well">
        <ul class="nav nav-pills nav-stacked">
              <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
              <li><a href="{{url('/personal')}}">Personal</a></li>
              <li><a href="{{url('/educational')}}">Educational Details</a></li>
              <li class="active"><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
              <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
              <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
              <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
              <li><a href="{{url('/referee')}}">Referees</a></li>
              <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
              <li><a href="{{url('/declaration')}}">Declaration</a></li>
              <li><a href="{{url('/final_print')}}">Final Print</a></li>
            </ul>
      </div>
      <div class="col-sm-9">
      @if(($exp13s))
        <table class="table table-bordered">
            <tr>
              <td>Designation</td><td>Scale of Pay and Present Basic & AGP</td><td>Name of Employers</td><td>From Data</td><td>To Date</td><td>No. of years/months</td><td>Nature of Work</td>
            </tr>
            @foreach ($exp13s as $exp13_data)
              <tr>
                <td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td><td>{{$exp13_data->designation}}</td>
              </tr>
            @endforeach
        </table>
      @endif 
      <form class="form-horizontal" method="post" action="{{ url('/postexp_13') }}">
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Designation</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="inputFname" name="designation">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Scale of Pay & Present Basic & AGP</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="inputFname" name="pay_scale">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Name and Address of the employer</label>
          <div class="col-sm-8">
            <textarea class="form-control" required id="inputFname" name="employer_details">
            </textarea>
          </div>
        </div>
        <h4>Period of experience</h4>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">From date</label>
          <div class="col-sm-8">
            <input type="text" id="dob" required class="form-control" id="inputFname" name="from_date">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">To date</label>
          <div class="col-sm-8">
            <input type="text" required id="dob1" class="form-control" id="inputFname" name="to_date">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">No. of years/months</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="inputFname" name="years_of_exp">
          </div>
        </div>
        <hr>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Nature of work/ duties</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="inputFname" name="nature_of_work">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">S. No. of proof of enclosure</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" id="inputFname" name="proof_sno">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-10 col-sm-2">
            <button type="submit" class="btn btn-block btn-success">Add</button></a>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
    </div>
  </div>
  <hr class="featurette-divider">
  </div>
@endsection
