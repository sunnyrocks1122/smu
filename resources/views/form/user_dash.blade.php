@extends('layouts.form_nonav')
@section('title','User Dashboard')
@section('content')
<div class="container">
  <h1>User Dashboard </h1>
  <hr>
  <div class="row" style="color:black;">
    <div class="col-lg-3 well">
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="{{url('/user_dash')}}">Dashboard</a></li>
          <li><a href="{{url('/personal')}}">Personal</a></li>
          <li><a href="{{url('/educational')}}">Educational Details</a></li>
          <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
          <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
          <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
          <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
          <li><a href="{{url('/referee')}}">Referees</a></li>
          <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
          <li><a href="{{url('/declaration')}}">Declaration</a></li>
          <li><a href="{{url('/final_print')}}">Final Print</a></li>
        </ul>
      </div>
      
	<div class="col-lg-9">
		<table class="table table-bordered">
	  	<tr>
	  		<td>User ID</td><td>{{session('user')->id}}</td>
	  	</tr>
	  	<tr>
	  		<td>Name</td><td>{{session('user')->first_name}} {{session('user')->middle_name}} {{session('user')->last_name}}</td>
	  	</tr>
	  	<tr>
	  		<td>Email</td><td>{{session('user')->email}}</td>
	  	</tr>
	  	<tr>
	  		<td>Post Applied For</td><td>{{session('user')->post_applied_for}}</td>
	  	</tr>
	</table>
	</div>
  </div> 
</div>  
<hr class="featurette-divider">
@endsection
