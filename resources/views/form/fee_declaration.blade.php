@extends('layouts.form_nonav')
@section('title','Declaration')
@section('content')
<div class="container">
  <h1>Fees and Declaration</h1>
  <hr>
  <div class="row" style="color:black;">
    <div class="col-lg-3 well">
        <ul class="nav nav-pills nav-stacked">
        <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
        <li><a href="{{url('/personal')}}">Personal</a></li>
        <li><a href="{{url('/educational')}}">Educational Details</a></li>
        <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
        <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
        <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
        <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
        <li><a href="{{url('/referee')}}">Referees</a></li>
        <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
        <li class="active"><a href="{{url('/declaration')}}">Declaration</a></li>
        <li><a href="{{url('/final_print')}}">Final Print</a></li>
      </ul>
      </div>
	<div class="col-lg-9">
    @if(Session::has('message'))
        <center><p class="alert {{ Session::get('alert-class', 'alert-danger') }}" id="msg11">{{ Session::get('message') }}</p></center>
    @endif
  
		<table class="table table-bordered">
	  	<tr>
	  		<td>Name</td><td>{{session('user')->first_name}} {{session('user')->middle_name}} {{session('user')->last_name}}</td>
	  	</tr>
	  	<tr>
	  		<td>Email</td><td>{{session('user')->email}}</td>
	  	</tr>
	  	<tr>
	  		<td>Name</td><td>{{session('user')->post_applied_for}}</td>
	  	</tr>
      @if(!($personal))
      <tr>
        <td colspan="2">Please fill the complete form first</td>
      </tr>
      @else
      <tr>
        <td>Have you applied earlier ? Advt. No. 19,21</td><td>{{$personal->previous_application}}</td>
      </tr>
      <tr>
        <td>Catagory</td><td>{{$personal->catagory}}</td>
      </tr>
      <tr>
        <td>Physically Disabled</td><td>{{$personal->disability}}</td>
      </tr>      
     @endif
	 </table>
   <table class="table table-bordered">
     <tr>
       <td>You had applied for {{session('user')->post_applied_for}}</td>
       <td>You have to pay 
       {{($fees)}}
       </td>
     </tr>
     <tr><td colspan="2">Details of Fee Payment (The requisite fee has to be remitted through RTGS/NEFT to Bank of Baroda, Mohaan Road, Lucknow, India on Account No. Saving A/C No. 36510100000025 & IFSC Code  BARB0MOHAAN (5th character is zero).</td></tr>
   </table>
   <hr>
   <h3 align="center">Declaration</h3>
   <div class="row">
    <div class="col-lg-12">
    <p>I,{{session('user')->first_name}} {{session('user')->middle_name}} {{session('user')->last_name}},Son/Daughter/Wife of, {{$guardian}} hereby declare that all statements and entries made in this application are true, complete and correct to the best of my knowledge and belief. It is further declared that I have never been convicted and no criminal case is pending or contemplated against me in any Court of law.</p><p>In the event of any information being found false or incorrect or ineligibility being detected before or after the Selection Committee and Executive Council meetings, my candidature / appointment may be cancelled by the University.
    </p>
    <div class="col-lg-6">
      <p></p>
      <p></p>
    </div>
    <div class="col-lg-6">
      
    </div>
    </div>
   </div>
	</div>
  </div> 
</div>  
<hr class="featurette-divider">
@endsection
