@extends('layouts.form_nonav')
@section('title','Educational Qualification')
@section('content')
  <h1 align="center">Academic Distrinctions</h1>
  <hr>
  <div class="row" style="color:black;">
          <div class="col-lg-3 well">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
              <li><a href="{{url('/personal')}}">Personal</a></li>
              <li><a href="{{url('/educational')}}">Educational Details</a></li>
              <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
              <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
              <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
              <li class="active"><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
              <li><a href="{{url('/referee')}}">Referees</a></li>
              <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
              <li><a href="{{url('/declaration')}}">Declaration</a></li>
              <li><a href="{{url('/final_print')}}">Final Print</a></li>
            </ul>
          </div>
          <div class="col-sm-9">
          @if(($ads))
          <table class="table table-bordered">
            <tr>
              <th>Name of the Academinc Course/Body</th><th>Academic Distinctions Obtained</th>
            </tr>
              @foreach ($ads as $ad)
                <tr>
                  <td>{{$ad->name_of_academy}}</td>
                  <td>{{$ad->academic_distinction_obtained}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        
      <form class="form-horizontal" action="{{url('/post_aca_dist')}}" method="post">
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Name of the Academinc Course/Body</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" name="name_of_academy" id="inputFname">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Academic Distinctions Obtained</label>
          <div class="col-sm-8">
            <input type="text" required class="form-control" name="academic_distinction_obtained" id="inputFname">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-10 col-sm-2">
            <button type="submit" class="btn btn-block btn-success">Add</button></a>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
    </div>
  </div>
  <hr class="featurette-divider">
@endsection
