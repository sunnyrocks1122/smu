@extends('layouts.form_nonav')
@section('title','Referees Details')
@section('content')
  <div class="container">
    <h1 align="center">Referees Details</h1>
    <hr>
    <div class="row" style="color:black;">
          <div class="col-lg-3 well">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
              <li><a href="{{url('/personal')}}">Personal</a></li>
              <li><a href="{{url('/educational')}}">Educational Details</a></li>
              <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
              <li><a href="{{url('/exp14')}}">Nature of experience</a></li>
              <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
              <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
              <li class="active"><a href="{{url('/referee')}}">Referees</a></li>
              <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
              <li><a href="{{url('/declaration')}}">Declaration</a></li>
              <li><a href="{{url('/final_print')}}">Final Print</a></li>
            </ul>
          </div>
        <div class="col-sm-9">
        <p>Names and complete postal addresses of 3 referees (The referee should be the last employers of the candidate or any other person having know-how of candidate's experience/ knowledge and should not be related to the applicant)</p>
        @if(($refs))
          <table class="table table-bordered">
            <tr>
              <th>Name</th><th>Address</th><th>Mobile No.</th><th>E-mail</th>
            </tr>
              @foreach ($refs as $ref)
                <tr>
                  <td>{{$ref->name}}</td>
                  <td>{{$ref->address}}</td>
                  <td>{{$ref->mobile}}</td>
                  <td>{{$ref->email}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        <form class="form-horizontal" action="{{url('/post_referee')}}" method="post">
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Name</label>
            <div class="col-sm-8">
              <input type="text" required class="form-control" name="name" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Mobile Number</label>
            <div class="col-sm-8">
              <input type="number" min="0" required class="form-control" name="mobile" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">E-Mail</label>
            <div class="col-sm-8">
              <input type="email" required class="form-control" name="email" id="inputFname">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-4 control-label">Address</label>
            <div class="col-sm-8">
              <textarea required class="form-control" name="address" id="inputFname"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-10 col-sm-2">
              <button type="submit" class="btn btn-block btn-success">Add</button></a>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
      </div>
    </div>
    <hr class="featurette-divider">
  </div>
@endsection
