@extends('layouts.form_nonav')
@section('title','Nature of Experience')
@section('content')
  <div class="container">
    <h1 align="center">Nature of Experience</h1>
    <hr>
    <div class="row" style="color:black;">
        <div class="col-lg-3 well">
          <ul class="nav nav-pills nav-stacked">
              <li><a href="{{url('/user_dash')}}">Dashboard</a></li>
              <li><a href="{{url('/personal')}}">Personal</a></li>
              <li><a href="{{url('/educational')}}">Educational Details</a></li>
              <li><a href="{{url('/exp13')}}">Chronological list of experience</a></li>
              <li class="active"><a href="{{url('/exp14')}}">Nature of experience</a></li>
              <li><a href="{{url('/exp15')}}">Details of Post-Doctoral Experience</a></li>
              <li><a href="{{url('/academic_distrinctions')}}">Academic Distinctions</a></li>
              <li><a href="{{url('/referee')}}">Referees</a></li>
              <li><a href="{{url('/publications')}}">Research, Publications and Academic Contributions</a></li>
              <li><a href="{{url('/declaration')}}">Declaration</a></li>
              <li><a href="{{url('/final_print')}}">Final Print</a></li>
            </ul>
        </div>
        <div class="col-sm-9">
          @if(($exp14s))
          <table class="table table-bordered">
            <tr>
              <th>Nature of experience</th><th>No. of years</th><th>No. of months</th>
            </tr>
              @foreach ($exp14s as $exp14)
                <tr>
                  <td>{{$exp14->nature_of_exp}}</td>
                  <td>{{$exp14->years}}</td>
                  <td>{{$exp14->months}}</td>
                </tr>
              @endforeach
          </table>
        @endif
        <form class="form-horizontal" method="post" action="{{url('/postexp_14')}}">
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Nature of Experience</label>
          <div class="col-sm-8">
            <select class="form-control" required name="nature_of_exp" id="nature_of_exp">
              <option value="">Please selece an option</option>
              <option value="Teaching Under-graduate level">Teaching Under-graduate level</option>
              <option value="Teaching Post-graduate level">Teaching Post-graduate level</option>
              <option value="Teaching Post-Doctoral level">Teaching Post-Doctoral level</option>
              <option value="Other experience">Other experience</option>
            </select> 
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">No. of years</label>
          <div class="col-sm-8">
            <input type="number" min="0" class="form-control" name="years" id="inputFname">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">No. of months</label>
          <div class="col-sm-8">
            <input type="number" min="0" class="form-control" name="months" id="inputFname">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-10 col-sm-2">
            <button type="submit" class="btn btn-block btn-success">Add</button></a>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
      </div>
    </div>
    <hr class="featurette-divider">
  </div>
@endsection
