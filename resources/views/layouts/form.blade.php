<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{URL::to('css/main.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{URL::to('css/one-page-wonder.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="font-family: 'Roboto Condensed', sans-serif;">
    <!-- Navigation -->
    @include('parts.mainnav')
    <!-- Full Width Image Header -->
        <div class="container">
          @yield('content')
        </div>
        <!-- Footer -->
        <div class="container">
          <footer>
              <div class="row">
                  <div class="col-lg-12">
                      <p>Copyright &copy; DSMNRU</p>
                  </div>
              </div>
          </footer>
        </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="{{URL::to('js/jquery.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{URL::to('js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/js/bootstrap-datepicker.min.js"></script>
    <script>
      $( function() {
        $( "#dob" ).datepicker({
                    format: "yyyy/mm/dd"
                });  
      } );
    </script>
    <script>
      $( function() {
        $( "#dob1" ).datepicker({
                    format: "yyyy/mm/dd"
                });  
      } );
    </script>

</body>
</html>
