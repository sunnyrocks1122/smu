<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{URL::to('css/main.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{URL::to('css/one-page-wonder.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .form-control{
      font-size:16px;
      color: black;
    }
    </style>
</head>
<body style="font-family: 'Roboto Condensed', sans-serif;">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Administrator Dashboard</a>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ url('/admin_login')}}">Log Out</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.container -->
    </nav>
    <!-- Full Width Image Header -->
        <div class="container">
            @yield('content')
        </div>
        <!-- Footer -->
    <!-- /.container -->
    <!-- jQuery -->
    <script  src="https://code.jquery.com/jquery-1.7.2.min.js" integrity="sha256-R7aNzoy2gFrVs+pNJ6+SokH04ppcEqJ0yFLkNGoFALQ=" crossorigin="anonymous"></script>
    <script src="{{URL::to('js/jquery.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{URL::to('js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#cat_sc").hide();
            $("#cat_others").hide();
            $("#disability_options").hide();
            $("#SC").click(function () {
                $("#cat_sc").show();
                $("#cat_others").hide();
            });
            $("#GEN").click(function () {
                $("#cat_sc").hide();
                $("#cat_others").hide();
            });
            $("#OBC").click(function () {
                $("#cat_sc").hide();
                $("#cat_others").hide();
            });
            $("#other").click(function () {
                $("#cat_sc").hide();
                $("#cat_others").show();
            });
            $("#other").click(function () {
                $("#cat_sc").hide();
                $("#cat_others").show();
            });
            $("#disable_n").click(function () {
                $("#disability_options").hide();
            });
            $("#disable_y").click(function () {
                $("#disability_options").show();
            });

        });
    </script>
</body>
<script type="text/javascript">
    $(function() {
    // setTimeout() function will be fired after page is loaded
    // it will wait for 5 sec. and then will fire
    // $("#successMessage").hide() function
        setTimeout(function() {
            $("#msg").hide()
        }, 5000);
    });
</script>
</html>
