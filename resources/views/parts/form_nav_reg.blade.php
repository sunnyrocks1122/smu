<div class="row">
  <div class="col-lg-12">
    <ul class="nav nav-pills" style="float:right">
      <li role="presentation"><a href="{{ url('/onlineform')}}"><button class="btn btn-block btn-lg btn-info"> Notices</button></a></li>
      <li role="presentation"><a href="{{ url('/new')}}"><button class="btn btn-block btn-lg btn-info"> New Registration</button></a></li>
      <li role="presentation"><a href="{{ url('/login_form')}}"><button class="btn btn-block btn-lg btn-info"> Login</button></a></li>
    </ul>
  </div>
</div>
