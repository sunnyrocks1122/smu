@extends('layouts.admin')
@section('title','Admin Dashboard')
@section('content')
  <hr>
  <h1 align="center">Admin Dashboard</h1>
  <hr>
  <div class="container">
    <div class="row" style="color:black;font-size:16px">
      <div class="col-lg-12">
        <h1>User Responses</h1>
        <table class="table table-bordered">
          <tr>
            <td>User ID</td>
            <td>Post Applied For</td>
            <td>Name</td>
            <td>Date of Birth</td>
            <td>Email</td>
            <td>Contact</td>
            <td>Options</td>
          </tr>
          @foreach ($user_data as $user)
          <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->post_applied_for}}</td>
            <td>{{$user->first_name}}</td>
            <td>{{$user->dob}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->password}}</td>
            <td><a href="#"><button class="btn btn-success">Edit</button></a></td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
  <hr class="featurette-divider">
  @endsection
