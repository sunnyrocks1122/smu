@extends('layouts.home')
@section('title','Log In')
@section('content')
  <div class="container">
    <h1 align="center">Admin Log In</h1>
    <hr>
    @if(Session::has('message'))
          <center><p style="center" class="alert {{ Session::get('alert-class', 'alert-danger') }}" id="msg">{{ Session::get('message') }}</p></center>
    @endif
    <div class="row" style="color:black;font-size:16px">      
      <div class="col-lg-12">
        <form class="form-horizontal" method="post" action="{{ url('/admin_postlogin') }}">
          <div class="form-group">
            <label for="inputNumber" class="col-sm-4 control-label">User Name</label>
            <div class="col-sm-8">
              <input type="text" required class="form-control" id="username" name="username">
            </div>
          </div>
          <div class="form-group">
            <label for="inputNumber" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8">
              <input type="text" required class="form-control" id="password" name="password">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-2">
              <button type="submit" class="btn btn-lg btn-success">Log In</button>
              <input type="hidden" name="_token" value="{{ Session::token() }}">
            </div>
          </div>
        </form>
      </div>
    </div>
    <hr class="featurette-divider">
  </div>
  @endsection
