@extends('layouts.form_nonav')
@section('title','Research Publications')
@section('content')
  <h1 align="center">CATEGORY III: RESEARCH, PUBLICATIONS AND ACADEMIC CONTRIBUTIONS</h1>
  <hr>
  <ol class="breadcrumb">
    <li><a href="{{ url('/personal')}}">Personal</a></li>
    <li><a href="{{ url('/educational')}}">Educatonal Details</a></li>
    <li><a href="{{ url('/exp13')}}">Experience</a></li>
    <li><a href="{{ url('/exp14')}}">Experience Details</a></li>
    <li><a href="{{ url('/exp15')}}">Post Doctoral Experience Details</a></li>
  </ol>
  <div class="row">
    <div class="col-lg-10">
      <h4>(A) (i) Published Papers in Refered Journals</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#papers">Add Papers</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="papers" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Published Papers in Refered Journals</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Journal</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">	Impact factor</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed ? Impact factor, if any	</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the first or corresponding author ?</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure Required</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-lg btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(A) (ii) Published Papers in Other Reputed Journals</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#articles">Add Articles</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="articles" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Published Papers in Other Reputed Journals</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Journal</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed ? Impact factor, if any</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the  first or corresponding author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-lg btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(B) Publications other than Journal articles (books, chapters in books etc.)<br>
      (i) Text/Reference, Books published by International Publishers, with ISBN/ISSN number</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#papers_pre">Add Papers</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="papers_pre" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Text/Reference, Books published by International Publishers</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Book Title, Editor & Publisher</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether Peer reviewed ?</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the first or corrosponding author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(B) (ii) Subject Books, published by National level publishers, with ISBN/ISSN number or State / Central Govt. Publications.</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#books">Add Books</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="books" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Books Published as author or as editor</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page numbers</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Details of Conference Publication</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Publisher & ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number of co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the main author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-lg btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(B) (iii) Subject Books, published by other local publishers, with ISBN/ISSN number</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#projects">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="projects" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Ongoing Projects/ Consultancies</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Type of Book & Authorship</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Publisher & ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">/Whether Peer reviewed ?</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">No. of Co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the main author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">APT Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
    <div class="row">
    <div class="col-lg-10">
      <h4>(B) (iv) Chapters in Books, published by National and International level publishers, with ISBN/ISSN number</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#b_iv">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="b_iv" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Ongoing Projects/ Consultancies</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title with Page No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Type of Book & Authorship</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Publisher & ISSN/ ISBN No.</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">/Whether Peer reviewed ?</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">No. of Co-authors</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether you are the main author</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">APT Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(C) Ongoing and Completed Research Projects and Consultancies 
      <br>(C) (i & ii) Ongoing Projects/Consultancies
      </h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Ongoing Projects/Consultancies</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organization</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Period</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Grant/ Amount Mobilized (Rs. Lakh)</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr><div class="row">
    <div class="col-lg-10">
      <h4>(C) (iii) Project Outcome/Outputs</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Project Outcome/Outputs</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether policy document/ patent/ technology transfer/ product/ process</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organization</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether International/ National/Central Government/ State Govt./Local bodies</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(D) Research Guidance</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Research Guidance</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Type</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="inputFname">
                        <option value="0">Please Select an Option</option>
                        <option value="M. Phil. or Equivalent">M. Phil. or Equivalent</option>
                        <option value="Ph. D. or Equivalent">Ph. D. or Equivalent</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Number Enrolled</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Thesis Submitted</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Degree awarded</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(E) (i) Fellowships/Awards from academic bodies/associations</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Fellowships/Awards from academic bodies/associations</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Name of the Award</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Academic body/Association</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Whether International/National/State/University level</label>
                    <div class="col-sm-8">
                      <select class="form-control" id="inputFname">
                        <option value="">Please Select</option>
                        <option value="International">International</option>
                        <option value="National">National</option>
                        <option value="State/University">State/University</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(F) Development of e-learning delivery process/material</h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Development of e-learning delivery process/material</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title of Module</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Recognized by/Submitted at/Delivered at</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h4>(C) Ongoing and Completed Research Projects and Consultancies 
      <br>(C) (i & ii) Ongoing Projects/Consultancies
      </h4>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#outcomes">Add</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="outcomes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Ongoing Projects/Consultancies</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organization</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Period</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Grant/ Amount Mobilized (Rs. Lakh)</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
                    <div class="col-sm-8">
                      <input type="number" min="0" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-lg-10">
      <h2>G. Paper presented in Conferences, Seminars, Workshops, Symposiam (mention only upto a maximum of Ten [10])</h2>
    </div>
    <div class="col-lg-2">
      <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#paper_presented">Add Papers</button>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" id="paper_presented" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Paper presented in Conferences, Seminars, Workshops, Symposiam</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="row" style="color:black;font-size:16px">
              <div class="col-lg-10">
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title of the Paper Presented </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Title of Conference / Seminar</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Organized by</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Type: International/ National/State/ Regional/College or University level</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Score</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">S. No. of proof of enclosure</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="inputFname">
                    </div>
                  </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a href="{{ url('/academic_distrinctions') }}"><button type="button" class="btn btn-success">Save and Next</button></a>
        </div>
      </div>
    </div>
  </div>
  <hr>
@endsection
