@extends('layouts.form_nonav')
@section('title','Research Publications')
@section('content')
	<div class="container">
    <h1 align="center">18. CATEGORY II: PROFESSIONAL DEVELOPMENT, CO-CURRICULAR AND EXTENSION ACTIVITIES</h1>
  <hr>
  <div class="row" style="color:black;">
    <div class="col-lg-3 well"></div>
	<div class="col-sm-9">
      <form class="form-horizontal" method="post" action="{{ url('/postexp_13') }}">
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Catagory</label>
          <div class="col-sm-8">
            <select required class="form-control" id="catagory" name="catagory">
            	<option value="">Please select</option>
            	<option value="Student related co-curricular, extension and field based activities">(a) Student related co-curricular, extension and field based activities</option>
            	<option value="Contribution to corporate life and management of the department and institution through participation in academic and administrative committees and responsibilities">(b) Contribution to corporate life and management of the department and institution through participation in academic and administrative committees and responsibilities</option>
            	<option value="Professional Development activities (such as participation in seminars, conferences, short term training courses, industrial experience, talks, lectures in refresher / faculty development courses, dissemination and general articles and any other contribution)">(c) Professional Development activities (such as participation in seminars, conferences, short term training courses, industrial experience, talks, lectures in refresher / faculty development courses, dissemination and general articles and any other contribution)</option>

            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Catagory</label>
          <div class="col-sm-8">
            <select required class="form-control" id="a" name="catagory">
            	<option value="">Please select</option>
            	<option value="Discipline related co-curricular activities (e.g. remedial classes, career counselling, study visit, student seminar and other events.)">Discipline related co-curricular activities (e.g. remedial classes, career counselling, study visit, student seminar and other events.)</option>
            	<option value="Other co-curricular activities (Cultural, Sports, NSS, NCC etc.)">Other co-curricular activities (Cultural, Sports, NSS, NCC etc.)</option>
            	<option value="Extension and dissemination activities (public /popular lectures/ talks/seminars etc.)">Extension and dissemination activities (public /popular lectures/ talks/seminars etc.)</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">Actual hours spent per academic year</label>
          <div class="col-sm-8">
            <input type="number" min="0" required class="form-control" id="inputFname" name="pay_scale">
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="col-sm-4 control-label">API Score</label>
          <div class="col-sm-8">
            <input type="number" min="0" required class="form-control" id="inputFname" name="pay_scale">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-10 col-sm-2">
            <button type="submit" class="btn btn-block btn-success">Add</button></a>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
          </div>
        </div>
      </form>
    </div>
  </div>
  <hr class="featurette-divider">
  </div>
@endsection
