@extends('layouts.form')
@section('title','Notices')
@section('content')
  <h1 align="center">Recruitment Portal</h1>
  <hr>
  <div class="row" style="color:black;">
  <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Downloads</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.                                            
                            </td>
                            <td>Advertisement</td>
                            <td>
                                <a href="{{url('/files/adv1.pdf')}}" target="_blank"><button class="btn btn-success">View</button></a>
                            </td>
                        </tr>

                        <tr>
                            <td>2.                                            
                            </td>
                            <td>Recruitment Details</td>
                            <td>
                                <a href="{{url('/files/r_detail1.pdf')}}" target="_blank"><button class="btn btn-success">View</button></a>
                            </td>
                        </tr>

                        <tr>
                            <td>3.                                            
                            </td>
                            <td>General Instructions</td>
                            <td>
                                <a href="{{url('/files/gi_adv22.pdf')}}" target="_blank"><button class="btn btn-success">View</button></a>
                            </td>
                        </tr>

                        <tr>
                            <td>4.                                            
                            </td>
                            <td>Steps Involved in the Application Process</td>
                            <td>
                                <a href="{{url('/files/steps_app_process.pdf')}}" target="_blank"><button class="btn btn-success">View</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>5.                                            
                            </td>
                            <td>New Registration</td>
                            <td>
                                <a href="{{url('/new')}}" target="_blank"><button class="btn btn-success">New Registration</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>6.                                            
                            </td>
                            <td>Already Registered</td>
                            <td>
                                <a href="{{url('/login')}}" target="_blank"><button class="btn btn-success">Login</button></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
</div>
@endsection
