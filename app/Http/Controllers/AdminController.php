<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
    	return view('admin.admin_login');
    }
    public function admin_dashboard()
    {
      return view('admin.admin_dashboard');
    }
    public function admin_postlogin(Request $request)
    {
      $username=$request['username'];
      $password=$request['password'];
      if($username=='admin@123'&&$password=='12345678')
      {
        return view('admin.admin_dashboard')->with(['user_data'=>User::all()]);;
      }
      else {
        $request->session()->flash('message', 'Please check User Name/Password');
        return redirect()->back();
      }
    }

}
