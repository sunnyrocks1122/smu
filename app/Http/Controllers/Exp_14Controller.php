<?php

namespace App\Http\Controllers;
use App\Exp_14;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Exp_14Controller extends Controller
{
    public function postexp_14(Request $request)
    {
       $user_id=Auth::user()->id;
       $nature_of_exp=$request['nature_of_exp'];
       $years=$request['years'];
       $months=$request['months'];
       
       $exp_14=new Exp_14();
       $exp_14->user_id=$user_id;
       $exp_14->nature_of_exp=$nature_of_exp;
       $exp_14->years=$years;
       $exp_14->months=$months;
       
       $exp_14->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/exp14');
    }
}
