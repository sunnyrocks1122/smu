<?php

namespace App\Http\Controllers;

use App\Personal_Details;
use App\User;
use App\Exp_13;
use App\Exp_14;
use App\Exp_15;
use App\Education1;
use App\Education2;
use App\Education3;
use App\Referee;
use App\Academic_Distinction;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DeleteController extends Controller
{
    public function del_referee($id)
    {
    	$ref = Referee::where('id',$id)->first();
        $ref->delete();
        //Session::flash('message', 'Successfully deleted the user!');
        return Redirect('/referee');
    }
}
