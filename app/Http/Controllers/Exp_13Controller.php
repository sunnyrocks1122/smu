<?php

namespace App\Http\Controllers;

use App\Exp_13;
use App\Education1;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Exp_13Controller extends Controller
{
    public function postexp_13(Request $request)
    {
	   $user_id=Auth::user()->id;
      $designation=$request['designation'];
      $pay_scale=$request['pay_scale'];
      $employer_details=$request['employer_details'];
      $from_date=$request['from_date'];
      $to_date=$request['to_date'];
      $years_of_exp=$request['years_of_exp'];
      $nature_of_work=$request['nature_of_work'];
      
       $exp_13=new Exp_13();
       $exp_13->user_id=$user_id;
       $exp_13->designation=$designation;
       $exp_13->pay_scale=$pay_scale;
       $exp_13->employer_details=$employer_details;
       $exp_13->from_date=$from_date;
       $exp_13->to_date=$to_date;
       $exp_13->years_of_exp=$years_of_exp;
       $exp_13->nature_of_work=$nature_of_work;
       
       $exp_13->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/exp13');
    }
}
