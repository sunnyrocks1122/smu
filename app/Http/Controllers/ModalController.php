<?php

namespace App\Http\Controllers;

use App\Personal_Details;
use App\User;
use App\Exp_13;
use App\Exp_14;
use App\Exp_15;
use App\Education1;
use App\Education2;
use App\Education3;
use App\Referee;
use App\AP_paper_in_conf_pro;
use App\Academic_Distinction;
use App\AP_published_paper_in_j;
use App\AP_article_published_in_book;
use App\AP_books_published_as_author;
use App\AP_ongoing_project;
use App\AP_paperpresented;
use App\AP_project_outcome;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ModalController extends Controller
{
    public function publications()
    {
    	$data_18as = AP_published_paper_in_j::where('user_id',Auth::user()->id)->get();
    	$data_18bs = AP_article_published_in_book::where('user_id',Auth::user()->id)->get();
      $data_18cs = AP_paper_in_conf_pro::where('user_id',Auth::user()->id)->get();
      $data_18ds = AP_books_published_as_author::where('user_id',Auth::user()->id)->get();
      $data_18es = AP_paper_in_conf_pro::where('user_id',Auth::user()->id)->get();
      $data_18fs = AP_project_outcome::where('user_id',Auth::user()->id)->get();
      $data_18gs = AP_paperpresented::where('user_id',Auth::user()->id)->get();
      if(!($data_18as)){
          return view('form.publications',['data_18as'=>'','data_18bs'=>'','data_18cs'=>'','data_18ds'=>'','data_18es'=>'','data_18fs'=>'','data_18gs'=>'','message'=>"No data",'user'=>Auth::user()]);
      }
      else
      {
          return view('form.publications',['data_18as'=>$data_18as,'data_18bs'=>$data_18bs,'data_18cs'=>$data_18cs,'data_18ds'=>$data_18ds,'data_18gs'=>$data_18gs,'data_18fs'=>$data_18fs,'data_18gs'=>$data_18gs,'user'=>Auth::user()]);
      }
    }
    public function post_18a(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18a'];
       $journal=$request['journal18a'];
       $issn=$request['issn18a'];
       $impact=$request['impact18a'];
       $peer_review=$request['peer_review18a'];
       $co_authors=$request['co_author18a'];
       $first_author=$request['first_author18a'];
       $score=$request['score18a'];
       
       $data_18a=new AP_published_paper_in_j();
       $data_18a->user_id=$user_id;
       $data_18a->title=$title;
       $data_18a->journal=$journal;
       $data_18a->issn=$issn;
       $data_18a->impact=$impact;
       $data_18a->peer_review=$peer_review;
       $data_18a->co_authors=$co_authors;
       $data_18a->first_author=$first_author;
       $data_18a->score=$score;

       $data_18a->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    public function post_18b(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18b'];
       $book_title=$request['book_title18b'];
       $issn=$request['issn18b'];
       $peer_review=$request['peer_review18b'];
       $co_authors=$request['co_authors18b'];
       $first_author=$request['first_author18b'];
       $score=$request['score18b'];
       
       $data_18b=new AP_article_published_in_book();
       $data_18b->user_id=$user_id;
       $data_18b->title=$title;
       $data_18b->book_title=$book_title;
       $data_18b->issn=$issn;
       $data_18b->peer_review=$peer_review;
       $data_18b->co_authors=$co_authors;
       $data_18b->first_author=$first_author;
       $data_18b->score=$score;

       $data_18b->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    public function post_18c(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18c'];
       $conf_publication=$request['conf_publication18c'];
       $issn=$request['issn18c'];
       $co_authors=$request['co_authors18c'];
       $main_author=$request['main_author18c'];
       $score=$request['score18c'];
       
       $data_18c=new AP_paper_in_conf_pro();
       $data_18c->user_id=$user_id;
       $data_18c->title=$title;
       $data_18c->conf_publication=$conf_publication;
       $data_18c->issn=$issn;
       $data_18c->co_authors=$co_authors;
       $data_18c->main_author=$main_author;
       $data_18c->score=$score;

       $data_18c->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    public function post_18d(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18d'];
       $type_of_authorship=$request['type_of_authorship18d'];
       $issn=$request['issn18d'];
       $peer_review=$request['peer_review18d'];
       $co_authors=$request['co_authors18d'];
       $main_author=$request['main_author18d'];
       $score=$request['score18d'];
       
       $data_18d=new AP_books_published_as_author();
       $data_18d->user_id=$user_id;
       $data_18d->title=$title;
       $data_18d->type_of_book=$type_of_authorship;
       $data_18d->peer_review=$peer_review;
       $data_18d->issn=$issn;
       $data_18d->co_authors=$co_authors;
       $data_18d->main_author=$main_author;
       $data_18d->score=$score;

       $data_18d->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    public function post_18e(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18e'];
       $organization=$request['organization18e'];
       $period=$request['period18e'];
       $grant=$request['grant18e'];
       $score=$request['score18e'];
       
       $data_18e=new AP_ongoing_project();
       $data_18e->user_id=$user_id;
       $data_18e->title=$title;
       $data_18e->organization=$organization;
       $data_18e->period=$period;
       $data_18e->grant=$grant;
       $data_18e->score=$score;

       $data_18e->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    public function post_18f(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18f'];
       $organization=$request['organization18f'];
       $period=$request['period18f'];
       $grant=$request['grant18f'];
       $policy_doc=$request['policy_doc18f'];
       $score=$request['score18f'];
       
       $data_18f=new AP_project_outcome();
       $data_18f->user_id=$user_id;
       $data_18f->title=$title;
       $data_18f->organization=$organization;
       $data_18f->grant=$grant;
       $data_18f->period=$period;
       $data_18f->policy_doc_as_outcome=$policy_doc;
       $data_18f->score=$score;

       $data_18f->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }public function post_18g(Request $request)
    {
       $user_id=Auth::user()->id;
       $title=$request['title18g'];
       $title_of_conf=$request['title_of_conf18g'];
       $organizer=$request['organizer18g'];
       $type_of_conf=$request['type_of_conf18g'];
       $score=$request['score18g'];
       
       $data_18g=new AP_paperpresented();
       $data_18g->user_id=$user_id;
       $data_18g->title=$title_of_conf;
       $data_18g->title_of_conference=$title_of_conf;
       $data_18g->organizer=$organizer;
       $data_18g->type=$type_of_conf;
       $data_18g->score=$score;

       $data_18g->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/publications');
    }
    
}