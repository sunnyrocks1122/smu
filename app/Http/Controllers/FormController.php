<?php

namespace App\Http\Controllers;

use App\Personal_Details;
use App\User;
use App\Exp_13;
use App\Exp_14;
use App\Exp_15;
use App\Education1;
use App\Education2;
use App\Education3;
use App\Referee;
use App\Academic_Distinction;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FormController extends Controller
{
    //
    public function index()
    {
    	return view('form.registration');
    }
    public function new()
    {
    	return view('form.new');
    }
    public function personal()
    {
    	$personal = Personal_Details::where('user_id',Auth::user()->id)->first();
        if(!($personal)){

            return view('form.personal',['msg'=>'Please Submit Profile Data','personal'=>'','user'=>Auth::user()]);
        }
        else
        {
            return view('form.personal',['personal'=>$personal,'user'=>Auth::user()]);
        }
    }
    public function educational()
    {
    	$edu1s = Education1::where('user_id1',Auth::user()->id)->get();
        $edu2s = Education2::where('user_id2',Auth::user()->id)->get();
        $edu3s = Education3::where('user_id3',Auth::user()->id)->get();
        if($edu1s){
            return view('form.educational',['edu1s'=>$edu1s,'edu3s'=>$edu3s,'edu2s'=>$edu2s,'user'=>Auth::user()]);
        }
        else
        {
            return view('form.personal',['message'=>'Please Submit Educational Data','edu1s'=>'','user'=>Auth::user()]);
        }
    }
    public function exp13()
    {
        $exp13s = Exp_13::where('user_id',Auth::user()->id)->get();
        if(!($exp13s)){
            return view('form.experience13',['exp13s'=>'','message'=>"No data",'user'=>Auth::user()]);
        }
        else
        {
            return view('form.experience13',['exp13s'=>$exp13s,'user'=>Auth::user()]);
        }
        return view('form.experience13');
    }
    public function exp14()
    {
        $exp14s = Exp_14::where('user_id',Auth::user()->id)->get();
        if(!($exp14s)){
            return view('form.experience14',['exp14s'=>'','message'=>"No data",'user'=>Auth::user()]);
        }
        else
        {
            return view('form.experience14',['exp14s'=>$exp14s,'user'=>Auth::user()]);
        }
    }
    public function exp15()
    {
        $exp15s = Exp_15::where('user_id',Auth::user()->id)->get();
        if(!($exp15s)){
            return view('form.experience15',['exp15s'=>'','message'=>"No data",'user'=>Auth::user()]);
        }
        else
        {
            return view('form.experience15',['exp15s'=>$exp15s,'user'=>Auth::user()]);
        }
    }
    public function academic_distrinctions()
    {
        $ads = Academic_Distinction::where('user_id',Auth::user()->id)->get();
        if(!($ads)){
            return view('form.academic_distrinctions',['ads'=>'','message'=>"No data",'user'=>Auth::user()]);
        }
        else
        {
            return view('form.academic_distrinctions',['ads'=>$ads,'user'=>Auth::user()]);
        }
    }
    public function referee()
    {
        $refs = Referee::where('user_id',Auth::user()->id)->get();
        if(!($refs)){
            return view('form.referee',['refs'=>'','message'=>"No data",'user'=>Auth::user()]);
        }
        else
        {
            return view('form.referee',['refs'=>$refs,'user'=>Auth::user()]);
        }
    }
    public function login_form()
    {
    	return view('form.login_form');
    }
    public function declaration()
    {
        $personal = Personal_Details::where('user_id',Auth::user()->id)->first();
        if(!($personal)){

            return view('form.fee_declaration',['msg'=>'Please Submit Profile Data','personal'=>'','fees'=>'0','user'=>Auth::user()]);
        }
        else
        {   
            if(($personal->spouse)=='')
            {
                $guardian=$personal->father_name;
            }
            else
            {
                $guardian=$personal->spouse;
            }    
            if($personal->disability=='Yes'){
                $fees='0';
            }
            else{
                if(Auth::user()->post_applied_for=='Assistant Professor')
                {
                    if($personal->catagory=='SC')
                    {
                        $fees=600;
                    }
                    else{
                        $fees=1200;
                    }
                }
                elseif(session('user')->post_applied_for=='Associate Professor'||session('user')->post_applied_for=='Professor')
                {
                    if($personal->catagory=='SC')
                    {
                        $fees=750;
                    }
                    else{
                        $fees=1300;
                    }
                }
            }
            return view('form.fee_declaration',['personal'=>$personal,'fees'=>$fees,'guardian'=>$guardian,'user'=>Auth::user()]);
        }
    }
    public function final_print()
    {
        return view('form.final_print');
    }
}
