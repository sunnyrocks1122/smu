<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exp_15;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Exp_15Controller extends Controller
{
    public function postexp_15(Request $request)
    {
       $user_id=Auth::user()->id;
       $agency=$request['agency'];
       $host_institution=$request['host_institution'];
       $from=$request['from'];
       $to=$request['to'];
       $duration=$request['duration'];
       
       $exp_15=new Exp_15();
       $exp_15->user_id=$user_id;
       $exp_15->agency=$agency;
       $exp_15->host_institution=$host_institution;
       $exp_15->from=$from;
       $exp_15->to=$to;
       $exp_15->duration=$duration;

       $exp_15->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/exp15');
    }

}
