<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use App\User;
use App\Exp_13;
use App\Exp_14;
use App\Exp_15;
use App\Personal_Details;
use App\Education1;
use App\Education2;
use App\Education3;
use App\Referee;
use App\Academic_Distinction;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function postReg(Request $request)
    {   
       function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
       }
       $rand_pass=generateRandomString();
       $rand1="1234";
       $post_applied_for=$request['post_applied_for'];
       $department=$request['department'];
       $nature_of_post=$request['nature_of_post'];
       $middle_name=$request['middle_name'];
       $first_name=$request['first_name'];
       $last_name=$request['last_name'];
       $dob=$request['dob'];
       $email=$request['email'];
       $contact=$request['contact'];

       $user=new User();
       $user->post_applied_for=$post_applied_for;
       $user->department=$department;
       $user->nature_of_post=$nature_of_post;
       $user->middle_name=$middle_name;
       $user->first_name=$first_name;
       $user->last_name=$last_name;
       $user->dob=$dob;
       $user->email=$email;
       $user->contact=$contact;
       $user->password=bcrypt($rand1);
       $user->save();

       $request->session()->flash('message', 'You are succesfully registered. Your id is '.$user->id.' and password is 1234');
       return redirect('/login_form');
    }
    public function postProfile(Request $request)
    {
       $user_id=Auth::user()->id;
       $father_name=$request['father_name'];
       $mother_name=$request['mother_name'];
       $placeofbirth=$request['placeofbirth'];
       $nationality=$request['nationality'];
       $gender=$request['gender'];
       $previous_application=$request['previous_application'];
       $status=$request['status'];
       $spouse=$request['spouse'];
       $catagory=$request['catagory'];
       $cat_certi_sn=$request['cat_certi_sn'];
       $other_cat_details=$request['other_cat_details'];
       $disability=$request['disability'];
       $disability_blindst=$request['disability_blindst'];
       $disability_blindper=$request['disability_blindper'];
       $disability_blindcerti=$request['disability_blindcerti'];
       $disability_hearingst=$request['disability_hearingst'];
       $disability_hearingper=$request['disability_hearingper'];
       $disability_hearingcerti=$request['disability_hearingcerti'];
       $disability_locomotorst=$request['disability_locomotorst'];
       $disability_locomotorper=$request['disability_locomotorper'];
       $disability_locomotorcerti=$request['disability_locomotorcerti'];
       $mailing_address=$request['mailing_address'];
       $permanent_address=$request['permanent_address'];
       
       $pu = Personal_Details::where('user_id',Auth::user()->id)->first();
       if(!($pu))
       {
         $pd=new Personal_Details();
         $pd->user_id=$user_id;
         $pd->previous_application=$previous_application;
         $pd->father_name=$father_name;
         $pd->mother_name=$mother_name;
         $pd->placeofbirth=$placeofbirth;
         $pd->nationality=$nationality;
         $pd->gender=$gender;
         $pd->status=$status;
         $pd->spouse=$spouse;
         $pd->catagory=$catagory;
         $pd->cat_certi_sn=$cat_certi_sn;
         $pd->other_cat_details=$other_cat_details;
         $pd->disability=$disability;
         $pd->disability_blindst=$disability_blindst;
         $pd->disability_blindper=$disability_blindper;
         $pd->disability_blindcerti=$disability_blindcerti;
         $pd->disability_hearingst=$disability_hearingst;
         $pd->disability_hearingper=$disability_hearingper;
         $pd->disability_hearingcerti=$disability_hearingcerti;
         $pd->disability_locomotorst=$disability_locomotorst;
         $pd->disability_locomotorper=$disability_locomotorper;
         $pd->disability_locomotorcerti=$disability_locomotorcerti;
         $pd->mailing_address=$mailing_address;
         $pd->permanent_address=$permanent_address;

         $pd->save();
         $request->session()->flash('message', 'Data submitted.');
         return redirect('/personal');
       }
       else
       {
         $pu->user_id=$user_id;
         $pu->father_name=$father_name;
         $pu->mother_name=$mother_name;
         $pu->previous_application=$previous_application;
         $pu->placeofbirth=$placeofbirth;
         $pu->nationality=$nationality;
         $pu->gender=$gender;
         $pu->status=$status;
         $pu->spouse=$spouse;
         $pu->catagory=$catagory;
         $pu->cat_certi_sn=$cat_certi_sn;
         $pu->other_cat_details=$other_cat_details;
         $pu->disability=$disability;
         $pu->disability_blindst=$disability_blindst;
         $pu->disability_blindper=$disability_blindper;
         $pu->disability_blindcerti=$disability_blindcerti;
         $pu->disability_hearingst=$disability_hearingst;
         $pu->disability_hearingper=$disability_hearingper;
         $pu->disability_hearingcerti=$disability_hearingcerti;
         $pu->disability_locomotorst=$disability_locomotorst;
         $pu->disability_locomotorper=$disability_locomotorper;
         $pu->disability_locomotorcerti=$disability_locomotorcerti;
         $pu->mailing_address=$mailing_address;
         $pu->permanent_address=$permanent_address; 
         $pu->save();;
         $request->session()->flash('message', 'Profile Updated.');
         return redirect('/personal');
       }
    }

    public function post_login(Request $request){

        if(Auth::attempt(['id'=>$request['user_id'],'password'=>$request['password']])){
            $user = User::where('id',$request->user_id)->first();
            session(['user' => $user]);            
            return redirect('/user_dash');
        }
        else{
            $user = User::where('id',$request->user_id)->first();
            $request->session()->flash('message', 'Please check ID/Password');
            return redirect()->back();
        }
    }
    
    public function log_out()
    {
      Auth::logout();
      session(['user' => '']);
      return redirect('/login')->with('message', 'Logged out succesfully');
    }
    public function user_dash()
    {
      return view('form.user_dash');
    }
    
    public function postEdu1(Request $request)
    {
       $user_id1=Auth::user()->id;
       $class1=$request['class1'];
       $course_name1=$request['course_name1'];
       $board_university1=$request['board_university1'];
       $year_of_passing1=$request['year_of_passing1'];
       $division1=$request['division1'];
       $CGPA1=$request['CGPA1'];
       $percentage1=$request['percentage1'];
       $subjects1=$request['subjects1'];
       
       $edu1=new Education1();
       $edu1->user_id1=$user_id1;
       $edu1->class1=$class1;
       $edu1->course_name1=$course_name1;
       $edu1->board_university1=$board_university1;
       $edu1->year_of_passing1=$year_of_passing1;
       $edu1->division1=$division1;
       $edu1->CGPA1=$CGPA1;
       $edu1->percentage1=$percentage1;
       $edu1->subjects1=$subjects1;

       $edu1->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/educational');
    }
    public function postEdu2(Request $request)
    {
       $user_id2=Auth::user()->id;
       $class2=$request['class2'];
       $course_name2=$request['course_name2'];
       $board_university2=$request['board_university2'];
       $year_of_passing2=$request['year_of_passing2'];
       $title2=$request['title2'];
       
       $edu2=new Education2();
       $edu2->user_id2=$user_id2;
       $edu2->class2=$class2;
       $edu2->course_name2=$course_name2;
       $edu2->board_university2=$board_university2;
       $edu2->year_of_passing2=$year_of_passing2;
       $edu2->title2=$title2;

       $edu2->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/educational');
    }
    public function postEdu3(Request $request)
    {
       $user_id3=Auth::user()->id;
       $subject3=$request['subject3'];
       $rollno3=$request['rollno3'];
       $year_of_passing3=$request['year_of_passing3'];
       
       $edu3=new Education3();
       $edu3->user_id3=$user_id3;
       $edu3->subject3=$subject3;
       $edu3->rollno3=$rollno3;
       $edu3->year_of_passing3=$year_of_passing3;

       $edu3->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/educational');
    }
    public function post_aca_dist(Request $request)
    {
       $user_id=Auth::user()->id;
       $name_of_academy=$request['name_of_academy'];
       $academic_distinction_obtained=$request['academic_distinction_obtained'];
       
       $ad=new Academic_Distinction();
       $ad->user_id=$user_id;
       $ad->name_of_academy=$name_of_academy;
       $ad->academic_distinction_obtained=$academic_distinction_obtained;

       $ad->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/academic_distrinctions');
    }
    public function post_referee(Request $request)
    {
       $user_id=Auth::user()->id;
       $name=$request['name'];
       $email=$request['email'];
       $mobile=$request['mobile'];
       $address=$request['address'];
       
       $referee=new Referee();
       $referee->user_id=$user_id;
       $referee->name=$name;
       $referee->address=$address;
       $referee->email=$email;
       $referee->mobile=$mobile;
       
       $referee->save();
       $request->session()->flash('message', 'You are data has been saved succesfully');
       return redirect('/referee');
    }
}
